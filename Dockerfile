FROM python:3.7

WORKDIR /app

RUN pip3 install redis flask
COPY app.py .

ENTRYPOINT ["python3", "app.py"]
