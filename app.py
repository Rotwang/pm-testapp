from flask import Flask
import redis
import socket
import os

hostname = socket.gethostname()

RELEASE = os.getenv('RELEASE')
REDIS_HOST = os.getenv('REDIS_{}_SERVICE_HOST'.format(RELEASE.upper())) # not the best to embedd this here but it's late

app = Flask(__name__)
redis = redis.Redis(REDIS_HOST)

@app.route("/")
def root():
    redis.incr("visited")
    c = int(redis.get("visited"))
    return f"hello! My hostname is {hostname} and this site was visited {c} times!"


@app.route("/healthz")
def healthz():
    """This only checks if the application is alive, it doesn't check redis connection"""
    return "OK"

@app.route("/readyz")
def readyz():
    """Is our app able to serve requests, aka check the redis conn"""
    redis.client_list()
    return "OK"

if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)
